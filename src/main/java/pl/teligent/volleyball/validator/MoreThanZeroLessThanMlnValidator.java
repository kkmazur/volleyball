package pl.teligent.volleyball.validator;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.ParameterException;

public class MoreThanZeroLessThanMlnValidator implements IValueValidator<Integer> {

	@Override
	public void validate(String name, Integer value) throws ParameterException {
		if(value == null) {
			return;
		}
		
		if(value.intValue() <= 0) {
			throw new ParameterException(name + " can't be less than or equal to 0");
		}
		if(value.intValue() > 1_000_000) {
			throw new ParameterException(name + " can't be more than 1 mln = 1 000 000");
		}
	}

}
