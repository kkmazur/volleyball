package pl.teligent.volleyball.cli;

import pl.teligent.volleyball.validator.MoreThanZeroLessThanMlnValidator;

import com.beust.jcommander.Parameter;

public class GameSettings {

	@Parameter(names = "-sets", description = "[optional] Number of games/sets", required = false, arity = 1, validateValueWith = MoreThanZeroLessThanMlnValidator.class)
	private Integer setCount = 3;

	@Parameter(names = "-winpoints", description = "[optional] Number of points to win a game/set", required = false, arity = 1, validateValueWith = MoreThanZeroLessThanMlnValidator.class)
	private Integer pointsToWin = 21;

	@Parameter(names = "-deciderpoints", description = "[optional] Number of points for the decider game/set", required = false, arity = 1, validateValueWith = MoreThanZeroLessThanMlnValidator.class)
	private Integer deciderGamePoints = 15;

	@Parameter(names = "-maxpoints", description = "[optional] Max number of points (e.g. first team reaching this number of points wins)", required = false, arity = 1, validateValueWith = MoreThanZeroLessThanMlnValidator.class)
	private Integer maxPoints;

	public GameSettings() {
	}

	public Integer getSetCount() {
		return setCount;
	}

	public void setSetCount(Integer setCount) {
		this.setCount = setCount;
	}

	public Integer getPointsToWin() {
		return pointsToWin;
	}

	public void setPointsToWin(Integer pointsToWin) {
		this.pointsToWin = pointsToWin;
	}

	public Integer getDeciderGamePoints() {
		return deciderGamePoints;
	}

	public void setDeciderGamePoints(Integer deciderGamePoints) {
		this.deciderGamePoints = deciderGamePoints;
	}

	public Integer getMaxPoints() {
		return maxPoints;
	}

	public void setMaxPoints(Integer maxPoints) {
		this.maxPoints = maxPoints;
	}

	@Override
	public String toString() {
		return "GameSettings [setCount=" + setCount + ", pointsToWin=" + pointsToWin + ", deciderGamePoints=" + deciderGamePoints + ", maxPoints=" + maxPoints + "]";
	}

}
