package pl.teligent.volleyball.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public class CLIReader {

	public GameSettings parseParams(String[] args) throws ParameterException {
		GameSettings settings = new GameSettings();
		new JCommander(settings, args);
		return settings;
	}
	
	public static String getUsageInfo() {
		GameSettings settings = new GameSettings();
		JCommander commander = new JCommander(settings, new String[] {});
		StringBuilder out = new StringBuilder();
		commander.usage(out);
		return out.toString();
	}
}
