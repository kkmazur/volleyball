package pl.teligent.volleyball;

import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import pl.teligent.volleyball.cli.CLIReader;
import pl.teligent.volleyball.cli.GameSettings;
import pl.teligent.volleyball.model.Game;
import pl.teligent.volleyball.model.Team;
import pl.teligent.volleyball.output.*;
import pl.teligent.volleyball.strategy.CLIInputScoreHitGeneratorStrategy;
import pl.teligent.volleyball.strategy.SimpleVolleyballStrategy;
import pl.teligent.volleyball.util.GameBuilder;

import com.beust.jcommander.ParameterException;

public class App {
	public static void main(String[] args) throws Exception {
		
		try(Scanner scanner = new Scanner(System.in)) {
			CLIReader reader = new CLIReader();
			GameSettings settings = reader.parseParams(args);
			System.out.println("Using settings: " + settings);
			
			String team1Name = "";
			String team2Name = "";
			while(team1Name.equals(team2Name)) {
				team1Name = readTeamName(scanner, 1);
				team2Name = readTeamName(scanner, 2);
				if(team1Name.equals(team2Name)) {
					System.out.println("Team names must be different");
				}
			}
			
			//@formatter:off
			DisplayEngine engine = new CLIDisplayEngine();
			Game game = new GameBuilder()
					.withSettings(settings)
					.withTeam1(new Team(team1Name))
					.withTeam2(new Team(team2Name))
					.withVolleyballStrategy(new SimpleVolleyballStrategy(new CLIInputScoreHitGeneratorStrategy(engine)))
					.withDisplayEngine(engine)
					.build();
			//@formatter:on
				
			game.display();
			while(!game.isOver()) {
				game.nextTurn();
			}
			
			game.showEndResults();
		}
		catch(ParameterException e) {
			System.out.println("Java Volleyball 1.0\n==================");
			System.out.println("Found invalid arguments!");
			System.out.println("\n==================");
			
			System.out.println(CLIReader.getUsageInfo());
		}
	}

	private static String readTeamName(Scanner scanner, int teamNumber) {
		System.out.printf("Please enter team %d name:\n", teamNumber);
		String teamName = "";
		while(StringUtils.isEmpty(teamName = scanner.nextLine().trim())) {
			System.out.println("Team name can't be blank");
		}
		return teamName;
	}
}
