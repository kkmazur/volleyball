package pl.teligent.volleyball.model;

import java.util.List;

import pl.teligent.volleyball.output.DisplayEngine;
import pl.teligent.volleyball.strategy.VolleyballStrategy;

public class Game {

	private GameState state;
	private VolleyballStrategy volleyballStrategy;
	private DisplayEngine displayEngine;

	public Game(GameState state, VolleyballStrategy volleyballStrategy, DisplayEngine displayEngine) {
		this.state = state;
		this.volleyballStrategy = volleyballStrategy;
		this.displayEngine = displayEngine;
	}

	public void display() {
		displayEngine.display(state);
	}

	public boolean isOver() {
		return state.isEndOfTheMatch();
	}

	public void nextTurn() {
		List<ActionMessage> messages = volleyballStrategy.serve(state);
		displayEngine.showMessages(messages);
		displayEngine.display(state);
	}

	public GameState getState() {
		return state;
	}

	public void showEndResults() {
		displayEngine.showEndResults(state);
	}

}
