package pl.teligent.volleyball.model;

import java.util.*;

import pl.teligent.volleyball.cli.GameSettings;
import pl.teligent.volleyball.util.CircularList;

public class GameState {

	private CircularList<TeamState> teams = new CircularList<>();
	private List<SetResult> setResults = new ArrayList<>();
	private GameSettings settings;
	private GamePart gamePart = GamePart.NORMAL_SET;

	private int currentSet = 0;
	private int currentServe = 0;

	public GameState(GameSettings settings, Team team1, Team team2) {
		Objects.requireNonNull(settings, "GameSettings can't be null");
		Objects.requireNonNull(team1, "Team1 can't be null");
		Objects.requireNonNull(team2, "Team2 can't be null");
		
		this.settings = settings;
		addTeam(team1);
		addTeam(team2);
	}

	private void addTeam(Team team) {
		teams.add(new TeamState(team));
	}

	public int getCurrentServe() {
		return currentServe;
	}

	public int getCurrentSet() {
		return currentSet;
	}

	public CircularList<TeamState> getTeams() {
		return teams;
	}

	public TeamState getCurrentTeam() {
		return teams.getCurrent();
	}

	public void changeServingTeam() {
		teams.next();
	}

	public void nextServe() {
		++this.currentServe;
	}
	
	public void nextSet() {
		++this.currentSet;
		saveSetResults();
		resetSetPoints();
		
		if(this.currentSet >= settings.getSetCount() && hasTeamsTheSameWinCount()) {
			gamePart = GamePart.DECIDER_SET;
		}
	}

	private boolean hasTeamsTheSameWinCount() {
		List<TeamState> sortedBySet = getSortedTeamsByWonSets();
		TeamState leader = sortedBySet.get(sortedBySet.size() - 1);
		TeamState second = sortedBySet.get(sortedBySet.size() - 2);
		boolean sameCount = leader.getSetsWon() == second.getSetsWon();
		return sameCount;
	}

	public boolean isEndOfTheSet() {
		Optional<TeamState> winningTeam = getWinningTeamInSet();
		return winningTeam.isPresent();
	}

	public Optional<TeamState> getWinningTeamInSet() {
		if(gamePart == GamePart.DECIDER_SET) {
			return teams.stream().filter((teamState) -> teamState.getPoints() >= settings.getDeciderGamePoints()).findAny();
		}
		return teams.stream().filter((teamState) -> teamState.getPoints() >= settings.getPointsToWin()).findAny();
	}

	public TeamState getWinningTeamInMatch() {
		List<TeamState> sorted = getSortedTeamsByTotalPoints();
		return sorted.get(sorted.size() - 1);
	}

	public boolean isEndOfTheMatch() {
		if(teams.size() < 2) {
			throw new IllegalStateException("Can't infere the end of the match when there are less than 2 teams");
		}
		List<TeamState> sortedBySet = getSortedTeamsByWonSets();
		List<TeamState> sortedByPoints = getSortedTeamsByTotalPoints();

		// 2 sets difference check
		TeamState leader = sortedBySet.get(sortedBySet.size() - 1);
		TeamState second = sortedBySet.get(sortedBySet.size() - 2);
		int setWinDifference = leader.getSetsWon() - second.getSetsWon();
		if(setWinDifference >= 2) {
			return true;
		}
		
		// max points reached check		
		TeamState leaderInPoints = sortedByPoints.get(sortedByPoints.size() - 1);
		if(settings.getMaxPoints() != null && leaderInPoints.getTotalPoints() >= settings.getMaxPoints()) {
			return true;
		}
		
		// deciding set check
		if(gamePart == GamePart.DECIDER_SET && !hasTeamsTheSameWinCount()) {
			return true;
		}
		
		return false;
	}

	private List<TeamState> getSortedTeamsByWonSets() {
		List<TeamState> sorted = new ArrayList<>(teams);
		Collections.sort(sorted, (a, b) -> Integer.valueOf(a.getSetsWon()).compareTo(b.getSetsWon()));
		return sorted;
	}
	
	private List<TeamState> getSortedTeamsByTotalPoints() {
		List<TeamState> sorted = new ArrayList<>(teams);
		Collections.sort(sorted, (a, b) -> Integer.valueOf(a.getTotalPoints()).compareTo(b.getTotalPoints()));
		return sorted;
	}

	public void setGamePart(GamePart gamePart) {
		this.gamePart = gamePart;
	}

	public boolean isDeciderSet() {
		return gamePart == GamePart.DECIDER_SET;
	}

	private void saveSetResults() {
		SetResult result = new SetResult();
		teams.forEach((team) -> {
			result.getResults().add(team.getPoints());
		});
		setResults.add(result);
	}
	
	public List<SetResult> getSetResults() {
		return Collections.unmodifiableList(setResults);
	}

	public void resetSetPoints() {
		teams.forEach((team) -> {
			team.resetSetPoints();
		});
	}

}
