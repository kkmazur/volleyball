package pl.teligent.volleyball.model;

public class TeamState {
	private Team team;
	private int totalPoints = 0;
	private int points = 0;
	private int setsWon = 0;

	public TeamState(Team team) {
		this.team = team;
	}

	public Team getTeam() {
		return team;
	}

	public int getTotalPoints() {
		return totalPoints;
	}

	public int getPoints() {
		return points;
	}

	public int getSetsWon() {
		return setsWon;
	}

	public void addPoints(int points) {
		if(points < 0) {
			throw new IllegalArgumentException("Points can't be removed from the team (points: " + points + ")");
		}
		this.points += points;
		this.totalPoints += points;
	}

	public void increaseSetsWon() {
		++setsWon;
	}

	public void resetSetPoints() {
		this.points = 0;
	}

}
