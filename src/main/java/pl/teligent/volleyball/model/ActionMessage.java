package pl.teligent.volleyball.model;

public class ActionMessage {

	private String message;

	public ActionMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
