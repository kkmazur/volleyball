package pl.teligent.volleyball.model;

public enum GamePart {

	NORMAL_SET, DECIDER_SET;
}
