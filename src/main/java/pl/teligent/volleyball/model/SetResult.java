package pl.teligent.volleyball.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author piotr
 *
 */
public class SetResult {

	private List<Integer> results = new ArrayList<>();

	public List<Integer> getResults() {
		return results;
	}

	@Override
	public String toString() {
		return results.toString();
	}

}
