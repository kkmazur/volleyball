package pl.teligent.volleyball.output;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import pl.teligent.volleyball.model.*;
import pl.teligent.volleyball.util.CircularList;

public class CLIDisplayEngine implements DisplayEngine {

	@Override
	public void display(GameState state) {
		printGameHeader(state.getCurrentSet(), state.getCurrentServe(), state.getCurrentTeam().getTeam().getName(), state.isDeciderSet());
		printHorizontalLine();
		printTeamPoints(state.getTeams());
		System.out.println("\n");
	}

	@Override
	public void showMessages(List<ActionMessage> messages) {
		messages.forEach((message) -> {
			System.out.println(message.getMessage());
		});
		System.out.println();
	}


	private void printHorizontalLine() {
		printHorizontalLine(true);
	}
	
	private void printHorizontalLine(boolean thick) {
		if(thick) {
			System.out.println("==============================================================================");
		}
		else {
			System.out.println("------------------------------------------------------------------------------");
		}
	}


	private void printGameHeader(int currentSet, int currentServe, String teamName, boolean deciderSet) {
		System.out.printf("SET: %5d\tSERVE: %5d (next serve by: %s, is decider set: %s)\n", currentSet, currentServe, teamName, deciderSet+"");
	}

	private void printTeamPoints(CircularList<TeamState> teams) {
		System.out.printf("%20s| %25s|\t%25s|\n", "Team name", "# of set wins", "# of points in this set");
		printHorizontalLine(false);
		teams.forEach((state) -> {
			System.out.printf("%20s| %25d|\t%25d| (%d)\n", state.getTeam().getName(), state.getSetsWon(), state.getPoints(), state.getTotalPoints());
		});
	}


	@Override
	public void showEndResults(GameState state) {
		TeamState winner = state.getWinningTeamInMatch();
		
		List<SetResult> setResults = state.getSetResults();
		int firstTeamScore = setResults.stream().map((r) -> r.getResults().get(0) > r.getResults().get(1) ? 1 : 0).reduce(Integer.valueOf(0), (sum, val) -> sum+val);
		int secondTeamScore = setResults.stream().map((r) -> r.getResults().get(0) < r.getResults().get(1) ? 1 : 0).reduce(Integer.valueOf(0), (sum, val) -> sum+val);
		
		if(winner.getSetsWon() == firstTeamScore) {
			String setScores = StringUtils.join(setResults.stream().map((r) -> r.getResults().get(0) + "-" + r.getResults().get(1)).collect(Collectors.toList()), ",");
			System.out.printf("Team %s won %d-%d (%s)\n", winner.getTeam().getName(), firstTeamScore, secondTeamScore, setScores);
		}
		else {
			String setScores = StringUtils.join(setResults.stream().map((r) -> r.getResults().get(1) + "-" + r.getResults().get(0)).collect(Collectors.toList()), ",");
			System.out.printf("Team %s won %d-%d (%s)\n", winner.getTeam().getName(), secondTeamScore, firstTeamScore, setScores);
		}
		
	}
}
