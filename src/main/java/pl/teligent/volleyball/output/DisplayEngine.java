package pl.teligent.volleyball.output;

import java.util.List;

import pl.teligent.volleyball.model.ActionMessage;
import pl.teligent.volleyball.model.GameState;

public interface DisplayEngine {

	void display(GameState state);
	
	void showMessages(List<ActionMessage> messages);

	void showEndResults(GameState state);
}
