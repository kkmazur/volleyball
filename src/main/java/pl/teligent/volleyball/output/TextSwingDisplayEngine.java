package pl.teligent.volleyball.output;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;



import javax.swing.*;

import org.apache.commons.lang3.StringUtils;



import pl.teligent.volleyball.model.*;
import pl.teligent.volleyball.util.CircularList;

public class TextSwingDisplayEngine implements DisplayEngine {

	private volatile JFrame frame;
	private volatile JTextArea area;

	public TextSwingDisplayEngine() throws Exception {
		SwingUtilities.invokeAndWait(() -> {
			frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			JPanel pane = new JPanel();
			pane.setLayout(new BorderLayout());
			frame.setContentPane(pane);

			area = new JTextArea();
			area.setMinimumSize(new Dimension(250, 200));
			pane.add(new JScrollPane(area), BorderLayout.CENTER);

			frame.pack();
			frame.setVisible(true);
			frame.requestFocus();
		});
	}
	
	@Override
	public void display(GameState state) {
		printGameHeader(state.getCurrentSet(), state.getCurrentServe(), state.getCurrentTeam().getTeam().getName(), state.isDeciderSet());
		printHorizontalLine();
		printTeamPoints(state.getTeams());
		area.append("\n");
	}

	@Override
	public void showMessages(List<ActionMessage> messages) {
		SwingUtilities.invokeLater(() -> {
			messages.forEach((message) -> {
				area.append(message.getMessage() + "\n");
			});
			area.append("\n\n");
		});

	}

	private void printHorizontalLine() {
		printHorizontalLine(true);
	}

	private void printHorizontalLine(boolean thick) {
		if(thick) {
			area.append("==============================================================================\n");
		}
		else {
			area.append("------------------------------------------------------------------------------\n");
		}
	}

	private void printGameHeader(int currentSet, int currentServe, String teamName, boolean deciderSet) {
		area.append(String.format("SET: %5d\tSERVE: %5d (next serve by: %s, is decider set: %s)\n", currentSet, currentServe, teamName, deciderSet + ""));
	}

	private void printTeamPoints(CircularList<TeamState> teams) {
		area.append(String.format("%20s| %25s|\t%25s|\n", "Team name", "# of set wins", "# of points in this set"));
		printHorizontalLine(false);
		teams.forEach((state) -> {
			area.append(String.format("%20s| %25d|\t%25d| (%d)\n", state.getTeam().getName(), state.getSetsWon(), state.getPoints(), state.getTotalPoints()));
		});
	}

	@Override
	public void showEndResults(GameState state) {
		TeamState winner = state.getWinningTeamInMatch();

		List<SetResult> setResults = state.getSetResults();
		int firstTeamScore = setResults.stream().map((r) -> r.getResults().get(0) > r.getResults().get(1) ? 1 : 0).reduce(Integer.valueOf(0), (sum, val) -> sum + val);
		int secondTeamScore = setResults.stream().map((r) -> r.getResults().get(0) < r.getResults().get(1) ? 1 : 0).reduce(Integer.valueOf(0), (sum, val) -> sum + val);

		if(winner.getSetsWon() == firstTeamScore) {
			String setScores = StringUtils.join(setResults.stream().map((r) -> r.getResults().get(0) + "-" + r.getResults().get(1)).collect(Collectors.toList()), ",");
			area.append(String.format("Team %s won %d-%d (%s)\n", winner.getTeam().getName(), firstTeamScore, secondTeamScore, setScores));
		}
		else {
			String setScores = StringUtils.join(setResults.stream().map((r) -> r.getResults().get(1) + "-" + r.getResults().get(0)).collect(Collectors.toList()), ",");
			area.append(String.format("Team %s won %d-%d (%s)\n", winner.getTeam().getName(), secondTeamScore, firstTeamScore, setScores));
		}

	}

}
