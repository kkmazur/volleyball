package pl.teligent.volleyball.strategy;

import pl.teligent.volleyball.model.GameState;

/**
 * @author Krzysztof Mazur <mazurkrzysztof.k@gmail.com>
 * 
 * A strategy pattern interface for checking if the serve by the current team will be a won or lost.
 *
 */
public interface ScoreHitGeneratorStrategy {

	boolean shouldScore(GameState state);
}
