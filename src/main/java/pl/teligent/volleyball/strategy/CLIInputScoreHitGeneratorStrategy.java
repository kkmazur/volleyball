package pl.teligent.volleyball.strategy;

import java.util.*;

import org.apache.commons.lang3.StringUtils;

import pl.teligent.volleyball.model.ActionMessage;
import pl.teligent.volleyball.model.GameState;
import pl.teligent.volleyball.output.DisplayEngine;

public class CLIInputScoreHitGeneratorStrategy implements ScoreHitGeneratorStrategy {

	private static enum Decision {
		YES("y", "yes"), NO("n", "no");

		private List<String> values;

		private Decision(String... values) {
			this.values = Arrays.asList(values);
		}

		public boolean matches(String str) {
			if(StringUtils.isEmpty(str)) {
				return false;
			}
			return values.contains(str);
		}
	}

	private DisplayEngine engine;

	public CLIInputScoreHitGeneratorStrategy(DisplayEngine engine) {
		this.engine = engine;
	}

	@Override
	public boolean shouldScore(GameState state) {
		engine.showMessages(Arrays.asList(new ActionMessage("Choose if the serving team should score this point:")));

		Scanner scanner = new Scanner(System.in);
		while(true) {
			String input = scanner.nextLine().trim();

			Optional<Decision> optional = Arrays.stream(Decision.values()).filter((e) -> e.matches(input)).findAny();
			if(!optional.isPresent()) {
				engine.showMessages(Arrays.asList(new ActionMessage("Please enter yes/y or no/n:")));
				continue;
			}

			Decision decision = optional.get();
			return decision == Decision.YES;
		}
	}
}
