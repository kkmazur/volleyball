package pl.teligent.volleyball.strategy;

import java.util.Random;

import pl.teligent.volleyball.model.GameState;

public class RandomScoreHitGeneratorStrategy implements ScoreHitGeneratorStrategy {
	
	private Random random = new Random();

	@Override
	public boolean shouldScore(GameState state) {
		return random.nextBoolean();
	}

}
