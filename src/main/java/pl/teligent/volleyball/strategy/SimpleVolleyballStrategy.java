package pl.teligent.volleyball.strategy;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.teligent.volleyball.model.*;

public class SimpleVolleyballStrategy implements VolleyballStrategy {

	private static final Logger LOG = LoggerFactory.getLogger(SimpleVolleyballStrategy.class);

	private ScoreHitGeneratorStrategy scoreHitStrategy;

	public SimpleVolleyballStrategy(ScoreHitGeneratorStrategy scoreHitStrategy) {
		this.scoreHitStrategy = scoreHitStrategy;
	}

	@Override
	public List<ActionMessage> serve(GameState state) {
		if(state.isEndOfTheMatch()) {
			throw new IllegalStateException("The game is already over!");
		}
		
		List<ActionMessage> messages = new ArrayList<>();
		state.nextServe();

		TeamState current = state.getCurrentTeam();
		String teamName = current.getTeam().getName();
		LOG.info("Team: {} is serving now", teamName);
		messages.add(new ActionMessage(teamName + " is serving now"));
		

		if(scoreHitStrategy.shouldScore(state)) {
			LOG.info("Team: {} won a point", teamName);
			messages.add(new ActionMessage(teamName + " won a point"));
			current.addPoints(1);
		}
		else {
			LOG.info("Team: {} lost a point", teamName);
			messages.add(new ActionMessage(teamName + " lost a point"));
			state.changeServingTeam();
			state.getCurrentTeam().addPoints(1);
		}

		if(state.isEndOfTheSet()) {
			messages.add(new ActionMessage("End of the set"));
			Optional<TeamState> teamStateOptional = state.getWinningTeamInSet();
			TeamState teamState = teamStateOptional.get();
			teamState.increaseSetsWon();
			state.nextSet();
		}
		
		return messages;

	}

}
