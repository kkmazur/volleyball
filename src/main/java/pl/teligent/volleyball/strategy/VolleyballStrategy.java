package pl.teligent.volleyball.strategy;

import java.util.List;

import pl.teligent.volleyball.model.ActionMessage;
import pl.teligent.volleyball.model.GameState;

/**
 * @author Krzysztof Mazur <mazurkrzysztof.k@gmail.com>
 * 
 * A strategy pattern interface for resolving how the single serve will change the game state & how the points are distributed 
 *
 */
public interface VolleyballStrategy {

	List<ActionMessage> serve(GameState state);
	
}
