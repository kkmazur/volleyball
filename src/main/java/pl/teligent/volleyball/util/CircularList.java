package pl.teligent.volleyball.util;

import java.util.ArrayList;
import java.util.Iterator;

public class CircularList<T> extends ArrayList<T> {

	private int currentIndex = 0;

	@Override
	public T get(int index) {
		if(size() == 0) {
			throw new IndexOutOfBoundsException();
		}
		return super.get(index % size());
	}

	@Override
	public Iterator<T> iterator() {
		throw new RuntimeException("Iterator is not supported for circular List");
	}

	/**
	 * Changes the internal <i>pointer</i> to the current object
	 * @return next value
	 */
	public T next() {
		++currentIndex;
		fixCurrentIndex();
		return get(currentIndex);
	}

	@Override
	public T remove(int index) {
		T value = super.remove(index);
		fixCurrentIndex();
		return value;
	}

	private void fixCurrentIndex() {
		if(currentIndex >= size()) {
			currentIndex = 0;
		}
	}

	public T getCurrent() {
		return get(currentIndex);
	}

}
