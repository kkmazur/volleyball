package pl.teligent.volleyball.util;

import java.util.Objects;

import pl.teligent.volleyball.cli.GameSettings;
import pl.teligent.volleyball.model.*;
import pl.teligent.volleyball.output.DisplayEngine;
import pl.teligent.volleyball.strategy.VolleyballStrategy;

public class GameBuilder {

	private GameSettings settings;
	private VolleyballStrategy volleyballStrategy;
	private DisplayEngine displayEngine;
	private Team team1;
	private Team team2;
	
	public GameBuilder withSettings(GameSettings settings) {
		this.settings = settings;
		return this;
	}
	
	public GameBuilder withVolleyballStrategy(VolleyballStrategy strategy) {
		this.volleyballStrategy = strategy;
		return this;
	}
	
	public GameBuilder withDisplayEngine(DisplayEngine engine) {
		this.displayEngine = engine;
		return this;
	}
	public GameBuilder withTeam1(Team team) {
		this.team1 = team;
		return this;
	}
	public GameBuilder withTeam2(Team team) {
		this.team2 = team;
		return this;
	}
	
	public Game build() {
		Objects.requireNonNull(team1);
		Objects.requireNonNull(team2);
		Objects.requireNonNull(settings);
		Objects.requireNonNull(volleyballStrategy);
		Objects.requireNonNull(displayEngine);
		
		GameState state = new GameState(settings, team1, team2);
		Game game = new Game(state, volleyballStrategy, displayEngine);
		
		return game;
	}
	
	
}
