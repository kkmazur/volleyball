package pl.teligent.volleyball.cli;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

import pl.teligent.volleyball.cli.CLIReader;
import pl.teligent.volleyball.cli.GameSettings;

import com.beust.jcommander.ParameterException;

public class CLIReaderTest {

	private CLIReader reader;

	@Before
	public void setUp() {
		reader = new CLIReader();
	}

	@Test
	public void emptyParamsListIsOk() throws ParseException {
		// given
		String[] args = new String[] {};

		// when
		GameSettings settings = reader.parseParams(args);

		//then
		assertThat(settings.getSetCount(), equalTo(3));
		assertThat(settings.getPointsToWin(), equalTo(21));
		assertThat(settings.getDeciderGamePoints(), equalTo(15));
		assertThat(settings.getMaxPoints(), is(nullValue()));
	}

	@Test(expected = ParameterException.class)
	public void invalidParamThrowsException() throws ParseException {
		// given
		String[] args = new String[] { "-sets", "X" };

		// when
		reader.parseParams(args);

		// throw exception
	}

	@Test
	public void allParametersAreSetAndCorrectIsOk() throws ParseException {
		// given
		String[] args = new String[] { "-sets", "10", "-winpoints", "15", "-deciderpoints", "5", "-maxpoints", "20" };

		// when
		GameSettings settings = reader.parseParams(args);

		//then
		assertThat(settings.getSetCount(), equalTo(10));
		assertThat(settings.getPointsToWin(), equalTo(15));
		assertThat(settings.getDeciderGamePoints(), equalTo(5));
		assertThat(settings.getMaxPoints(), equalTo(20));
	}
	
	@Test
	public void allButMaxPointsParametersAreSetAndCorrectIsOk() throws ParseException {
		// given
		String[] args = new String[] { "-sets", "10", "-winpoints", "15", "-deciderpoints", "5" };

		// when
		GameSettings settings = reader.parseParams(args);

		//then
		assertThat(settings.getSetCount(), equalTo(10));
		assertThat(settings.getPointsToWin(), equalTo(15));
		assertThat(settings.getDeciderGamePoints(), equalTo(5));
		assertThat(settings.getMaxPoints(), is(nullValue()));
	}
	
	@Test(expected = ParameterException.class)
	public void negativeValuesShouldThrowException() throws ParseException {
		// given
		String[] args = new String[] { "-sets", "10", "-winpoints", "15", "-deciderpoints", "-5" };
		
		// when
		reader.parseParams(args);
		
		//then throw exception
	}
	
	@Test(expected = ParameterException.class)
	public void toBigValuesShouldThrowException() throws ParseException {
		// given
		String[] args = new String[] { "-sets", "10", "-winpoints", "1000001", "-deciderpoints", "3" };
		
		// when
		reader.parseParams(args);
		
		//then throw exception
	}
	

}
