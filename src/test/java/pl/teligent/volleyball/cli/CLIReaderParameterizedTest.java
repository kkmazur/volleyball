package pl.teligent.volleyball.cli;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pl.teligent.volleyball.cli.CLIReader;
import pl.teligent.volleyball.cli.GameSettings;


@RunWith(Parameterized.class)
public class CLIReaderParameterizedTest {

	private CLIReader reader;
	
	private String[] args;
	
	public CLIReaderParameterizedTest(String[] args) {
		this.args = args;
		this.reader = new CLIReader();
	}

	@Parameters
	public static Collection<String[][]> params() {
		//@formatter:off
		String[][][] data = new String[][][] { 
				new String[][] { { "-sets", "10", "-winpoints", "15", "-deciderpoints", "5", "-maxpoints", "20" } },
				new String[][] { { "-sets", "1", "-winpoints", "2", "-deciderpoints", "3", "-maxpoints", "4" } },
				new String[][] { { "-sets", "4", "-winpoints", "3", "-deciderpoints", "2", "-maxpoints", "1" } },
				new String[][] { { "-sets", "10000", "-winpoints", "236123", "-deciderpoints", "234624", "-maxpoints", "133322" } },
			};
		//@formatter:on
		return Arrays.asList(data);
	}

	@Test
	public void allParametersAreSetAndCorrectIsOk() throws ParseException {
		// given
		// this.args

		// when
		GameSettings settings = reader.parseParams(this.args);

		//then
		assertTrue(settings.getSetCount() > 0);
		assertTrue(settings.getPointsToWin() > 0);
		assertTrue(settings.getDeciderGamePoints() > 0);
		assertTrue(settings.getMaxPoints() > 0);
	}

}
