package pl.teligent.volleyball;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import pl.teligent.volleyball.cli.CLIReaderParameterizedTest;
import pl.teligent.volleyball.cli.CLIReaderTest;
import pl.teligent.volleyball.game.GameScenariosTest;
import pl.teligent.volleyball.game.GameWinTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	CLIReaderParameterizedTest.class,
	CLIReaderTest.class,
	GameScenariosTest.class,
	GameWinTest.class
})

public class AllTests {
}