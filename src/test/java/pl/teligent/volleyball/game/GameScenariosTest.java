package pl.teligent.volleyball.game;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.core.IsEqual;
import org.junit.Test;

import pl.teligent.volleyball.cli.GameSettings;
import pl.teligent.volleyball.model.*;
import pl.teligent.volleyball.strategy.*;

public class GameScenariosTest {

	@Test
	public void team2WinsByDeciderSet() {
		// setup
		GameSettings settings = new GameSettings();
		settings.setSetCount(4);
		settings.setDeciderGamePoints(5);
		settings.setMaxPoints(15);
		settings.setPointsToWin(3);
		GameState gameState = new GameState(settings, new Team("Team 1"), new Team("Team 2"));
		
		//@formatter:off
		boolean[] scores = new boolean[] {
				true, true, false,	// Team 1
					true, false, // Team 2, and so on
				// -- next set
				false,
					true, true,
				// --
					true, false,
				false,
					true,
				// --
					false, 
				true, true,
				// -- DECIDER SET
				false,
					true, true, false,
				true, true, true, false,
					true
				
		};
		//@formatter:on
		VolleyballStrategy strategy = new SimpleVolleyballStrategy(new PredefinedScoreHitGeneratorStrategy(scores));

		for(int i = 0; i < scores.length; ++i) {
			strategy.serve(gameState);
		}
		
		TeamState winnerTeam = gameState.getWinningTeamInMatch();
		assertThat(winnerTeam.getTotalPoints(), IsEqual.equalTo(13));
		assertThat(winnerTeam.getTeam().getName(), IsEqual.equalTo("Team 2"));
		assertTrue(gameState.isDeciderSet());
		List<SetResult> results = gameState.getSetResults();
		assertThat(results.size(), IsEqual.equalTo(5));
		assertThat(results.get(0).getResults(), IsEqual.equalTo(Arrays.asList(3, 2)));
		assertThat(results.get(1).getResults(), IsEqual.equalTo(Arrays.asList(0, 3)));
		assertThat(results.get(2).getResults(), IsEqual.equalTo(Arrays.asList(1, 3)));
		assertThat(results.get(3).getResults(), IsEqual.equalTo(Arrays.asList(3, 0)));
		assertThat(results.get(4).getResults(), IsEqual.equalTo(Arrays.asList(4, 5)));
		assertTrue(gameState.isEndOfTheMatch());
	}
	
	@Test
	public void team1WinsBySetWin() {
		// setup
		GameSettings settings = new GameSettings();
		settings.setSetCount(3);
		settings.setDeciderGamePoints(5);
		settings.setMaxPoints(15);
		settings.setPointsToWin(3);
		GameState gameState = new GameState(settings, new Team("Team 1"), new Team("Team 2"));
		
		//@formatter:off
		boolean[] scores = new boolean[] {
				true, true, true,	// Team 1
				// -- 
				true, true, true
		};
		//@formatter:on
		VolleyballStrategy strategy = new SimpleVolleyballStrategy(new PredefinedScoreHitGeneratorStrategy(scores));

		for(int i = 0; i < scores.length; ++i) {
			strategy.serve(gameState);
		}
		
		TeamState winnerTeam = gameState.getWinningTeamInMatch();
		assertThat(winnerTeam.getTotalPoints(), IsEqual.equalTo(6));
		assertThat(winnerTeam.getTeam().getName(), IsEqual.equalTo("Team 1"));
		assertFalse(gameState.isDeciderSet());
		List<SetResult> results = gameState.getSetResults();
		assertThat(results.size(), IsEqual.equalTo(2));
		assertThat(results.get(0).getResults(), IsEqual.equalTo(Arrays.asList(3, 0)));
		assertThat(results.get(1).getResults(), IsEqual.equalTo(Arrays.asList(3, 0)));
		assertTrue(gameState.isEndOfTheMatch());
	}
	
	
}
