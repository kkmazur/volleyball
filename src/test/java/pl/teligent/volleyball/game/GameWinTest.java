package pl.teligent.volleyball.game;

import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.IntStream;

import org.hamcrest.core.IsEqual;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pl.teligent.volleyball.cli.GameSettings;
import pl.teligent.volleyball.model.*;
import pl.teligent.volleyball.strategy.PredefinedScoreHitGeneratorStrategy;
import pl.teligent.volleyball.strategy.SimpleVolleyballStrategy;

@RunWith(Parameterized.class)
public class GameWinTest {

	@Parameters
	public static Collection<Object[]> params() {
		//@formatter:off
		// pointsToWin, winningTeamName, setsWon, totalPoints, scoreData
		Object[][] data = new Object[][] { 
			 { 3, "Team 1", 2, 6, new boolean[] { true, true, true, false, true, true, false, true, true } },
			 { 2, "Team 1", 2, 4, new boolean[] { true, true, false, true, true, false, true } },
			 { 2, "Team 2", 2, 4, new boolean[] { true, false, true, true, false, false} }
			};
		//@formatter:on
		return Arrays.asList(data);
	}

	private GameState state;
	private String winningTeam;
	private Integer setsWon;
	private Integer totalPoints;
	private SimpleVolleyballStrategy strategy;
	private boolean[] scores;

	public GameWinTest(Integer pointsToWin, String winningTeam, Integer setsWon, Integer totalPoints, boolean[] scores) {
		this.winningTeam = winningTeam;
		this.setsWon = setsWon;
		this.totalPoints = totalPoints;
		this.scores = scores;

		GameSettings settings = new GameSettings();
		settings.setPointsToWin(pointsToWin);

		this.state = new GameState(settings, new Team("Team 1"), new Team("Team 2"));

		strategy = new SimpleVolleyballStrategy(new PredefinedScoreHitGeneratorStrategy(scores));
	}

	@Test
	public void simpleStraightWin() {
		IntStream.range(0, scores.length).forEach((i) -> {
			strategy.serve(state);
		});

		TeamState winner = state.getWinningTeamInMatch();
		assertThat(winner.getSetsWon(), IsEqual.equalTo(setsWon));
		assertThat(winner.getTeam().getName(), IsEqual.equalTo(winningTeam));
		assertThat(winner.getTotalPoints(), IsEqual.equalTo(totalPoints));
	}
}
