package pl.teligent.volleyball.strategy;

import pl.teligent.volleyball.model.GameState;
import pl.teligent.volleyball.strategy.ScoreHitGeneratorStrategy;

public class PredefinedScoreHitGeneratorStrategy implements ScoreHitGeneratorStrategy {
	private int index;
	private boolean[] scores;
	
	public PredefinedScoreHitGeneratorStrategy(boolean[] scores) {
		this.scores = scores;
	}

	@Override
	public boolean shouldScore(GameState state) {
		return scores[index++];
	}
}
